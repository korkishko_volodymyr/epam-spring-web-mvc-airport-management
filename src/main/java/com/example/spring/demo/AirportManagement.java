package com.example.spring.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirportManagement {
    public static void main(String[] args) {
        SpringApplication.run(AirportManagement.class, args);
    }
}

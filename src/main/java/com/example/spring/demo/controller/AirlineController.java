package com.example.spring.demo.controller;

import com.example.spring.demo.model.Airline;
import com.example.spring.demo.service.contract.AirlineService;
import com.example.spring.demo.service.contract.AirportService;
import com.example.spring.demo.service.impl.AirlineServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AirlineController {

    private final AirlineService airlineService;
    private final AirportService airportService;

    public AirlineController(AirlineServiceImpl airlineService, AirportService airportService) {
        this.airlineService = airlineService;
        this.airportService = airportService;
    }

    @GetMapping("/airline")
    public String getAll(Model model) {
        model.addAttribute("airlines", airlineService.getAll());
        return "airline";
    }

    @GetMapping("/addAirline")
    public String getAddPage(Model model) {
        model.addAttribute("airline", new Airline());
        model.addAttribute("airportList" , airportService.getAll());
        return "airlineAction";
    }

    @GetMapping("/updateAirline")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("airportList" , airportService.getAll());
        model.addAttribute("airline", airlineService.findById(id));
        return "airlineAction";
    }

    @GetMapping("/deleteAirline")
    public String delete(@RequestParam Integer id) {
        airlineService.delete(id);
        return "redirect:/airline";
    }

    @PostMapping("/addAirline")
    public String create(@ModelAttribute("airline") Airline airline, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        airlineService.save(airline);
        return "redirect:/airline";
    }

    @PostMapping("/updateAirline")
    public String getUpdateAirlinePage(@ModelAttribute("airline") Airline airline, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        airlineService.update(airline);
        return "redirect:/airline";
    }
}

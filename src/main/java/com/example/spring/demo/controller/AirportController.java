package com.example.spring.demo.controller;

import com.example.spring.demo.model.Airport;
import com.example.spring.demo.service.contract.AirportService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

@Controller
public class AirportController {

    private final AirportService airportService;

    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping("/airport")
    public String getAll(Model model) {
        model.addAttribute("airports", airportService.getAll());
        return "airport";
    }

    @GetMapping("/addAirport")
    public String getAddPage(Model model) {
        model.addAttribute("airport", new Airport());
        model.addAttribute("countries", getCountriesMap());
        return "airportAction";
    }

    @GetMapping("/updateAirport")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("airport", airportService.findById(id));
        model.addAttribute("countries", getCountriesMap());
        return "airportAction";
    }

    @GetMapping("/deleteAirport")
    public String delete(@RequestParam Integer id) {
        airportService.delete(id);
        return "redirect:/airport";
    }

    @PostMapping("/addAirport")
    public String create(@ModelAttribute("airport") Airport airport, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        airportService.save(airport);
        return "redirect:/airport";
    }

    @PostMapping("/updateAirport")
    public String getUpdateAirlinePage(@ModelAttribute("airport") Airport airport, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        airportService.update(airport);
        return "redirect:/airport";
    }

    private Map<String, String> getCountriesMap(){
        String[] countryCodes = Locale.getISOCountries();
        Map<String, String> mapCountries = new TreeMap<>();
        for (String countryCode : countryCodes) {
            Locale locale = new Locale("", countryCode);
            String code = locale.getCountry();
            String name = locale.getDisplayCountry();
            mapCountries.put(code, name);
        }
        return mapCountries;
    }
}

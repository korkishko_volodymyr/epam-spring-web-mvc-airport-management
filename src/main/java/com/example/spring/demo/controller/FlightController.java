package com.example.spring.demo.controller;

import com.example.spring.demo.model.Flight;
import com.example.spring.demo.service.contract.AirportService;
import com.example.spring.demo.service.contract.FlightService;
import com.example.spring.demo.service.contract.PlaneService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FlightController {

    private final FlightService flightService;
    private final PlaneService planeService;

    public FlightController(FlightService flightService, PlaneService planeService)  {
        this.flightService = flightService;
        this.planeService = planeService;
    }

    @GetMapping("/flight")
    public String getAll(Model model) {
        final var flightList = flightService.getAll();
        model.addAttribute("flights", flightList);
        return "flight";
    }

    @GetMapping("/updateFlight")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("planeList" , planeService.getAll());
        model.addAttribute("flight", flightService.findById(id));
        return "flightAction";
    }

    @GetMapping("/addFlight")
    public String getAddPage(Model model) {
        model.addAttribute("flight", new Flight());
        model.addAttribute("planeList" , planeService.getAll());
        return "flightAction";
    }

    @GetMapping("/deleteFlight")
    public String delete(@RequestParam Integer id) {
        flightService.delete(id);
        return "redirect:/flight";
    }

    @PostMapping("/addFlight")
    public String create(@ModelAttribute("flight") Flight flight, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        flightService.save(flight);
        return "redirect:/flight";
    }

    @PostMapping("/updateFlight")
    public String getUpdateAirlinePage(@ModelAttribute("flight") Flight flight, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        flightService.update(flight);
        return "redirect:/flight";
    }
}

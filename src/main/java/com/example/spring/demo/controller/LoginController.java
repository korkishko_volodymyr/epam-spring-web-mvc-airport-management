package com.example.spring.demo.controller;

import com.example.spring.demo.model.Admin;
import com.example.spring.demo.model.exception.FailedLoginException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {

    private static final String LOGIN = "korkishko1998";
    private static final String PASSWORD = "epamLviv";
    private static final String INVALID_LOGIN_MESSAGE = "Wrong Login or Password!";
    private static final Admin realAdmin = new Admin(LOGIN, PASSWORD);

    @GetMapping
    public String getLoginPage(Model model) {
        model.addAttribute("admin", new Admin());
        return "login";
    }

    @PostMapping
    public String login(@ModelAttribute("admin") Admin admin, BindingResult result, Model model){
        if (result.hasErrors()) {
            return "error";
        }
        try {
            login(admin);
        } catch (FailedLoginException e) {
            model.addAttribute("message" , INVALID_LOGIN_MESSAGE);
            return "login";
        }
        return "redirect:/ticket";
    }

    private void login(Admin admin) throws FailedLoginException {
        if (!(admin.equals(realAdmin))) {
            throw new FailedLoginException("Login failed");
        }
    }
}

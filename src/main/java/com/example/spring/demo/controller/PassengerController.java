package com.example.spring.demo.controller;

import com.example.spring.demo.model.Passenger;
import com.example.spring.demo.service.contract.PassengerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PassengerController {

    private final PassengerService passengerService;

    public PassengerController(PassengerService passengerService) {
        this.passengerService = passengerService;
    }

    @GetMapping("/passenger")
    public String getAll(Model model) {
        model.addAttribute("passengers", passengerService.getAll());
        return "passenger";
    }

    @GetMapping("/addPassenger")
    public String getAddPage(Model model) {
        model.addAttribute("passenger", new Passenger());
        return "passengerAction";
    }

    @GetMapping("/updatePassenger")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("passenger", passengerService.findById(id));
        return "passengerAction";
    }

    @GetMapping("/deletePassenger")
    public String delete(@RequestParam Integer id) {
        passengerService.delete(id);
        return "redirect:/passenger";
    }
    @PostMapping("/addPassenger")
    public String create(@ModelAttribute("passenger") Passenger passenger, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        passengerService.save(passenger);
        return "redirect:/passenger";
    }

    @PostMapping("/updatePassenger")
    public String getUpdateAirlinePage(@ModelAttribute("passenger") Passenger passenger, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        passengerService.update(passenger);
        return "redirect:/passenger";
    }
}

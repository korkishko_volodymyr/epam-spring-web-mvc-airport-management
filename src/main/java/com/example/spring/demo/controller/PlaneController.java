package com.example.spring.demo.controller;

import com.example.spring.demo.model.Airline;
import com.example.spring.demo.model.Plane;
import com.example.spring.demo.service.contract.AirlineService;
import com.example.spring.demo.service.contract.PlaneService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class PlaneController {

    private final PlaneService planeService;
    private final AirlineService airlineService;

    public PlaneController(PlaneService planeService, AirlineService airlineService) {
        this.planeService = planeService;
        this.airlineService = airlineService;
    }

    @GetMapping("/plane")
    public String getAll(Model model) {
        model.addAttribute("planes", planeService.getAll());
        return "plane";
    }

    @GetMapping("/addPlane")
    public String getAddPage(Model model) {
        model.addAttribute("plane", new Plane());
        model.addAttribute("airlineList" , airlineService.getAll());
        return "planeAction";
    }

    @GetMapping("/updatePlane")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("airlineList" , airlineService.getAll());
        model.addAttribute("plane", planeService.findById(id));
        return "planeAction";
    }

    @GetMapping("/deletePlane")
    public String delete(@RequestParam Integer id) {
        planeService.delete(id);
        return "redirect:/plane";
    }

    @PostMapping("/addPlane")
    public String create(@ModelAttribute("plane") Plane plane, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        planeService.save(plane);
        return "redirect:/plane";
    }

    @PostMapping("/updatePlane")
    public String getUpdateAirlinePage(@ModelAttribute("plane") Plane plane, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        planeService.update(plane);
        return "redirect:/plane";
    }
}

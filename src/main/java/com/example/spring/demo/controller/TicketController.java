package com.example.spring.demo.controller;

import com.example.spring.demo.model.Ticket;
import com.example.spring.demo.service.contract.PassengerService;
import com.example.spring.demo.service.contract.TicketService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class TicketController {

    private final TicketService ticketService;
    private final PassengerService passengerService;

    public TicketController(TicketService ticketService, PassengerService passengerService) {
        this.ticketService = ticketService;
        this.passengerService = passengerService;
    }

    @GetMapping("/ticket")
    public String getAll(Model model) {
        final var ticketList = ticketService.getAll();
        model.addAttribute("tickets", ticketList);
        return "ticket";
    }

    @GetMapping("/updateTicket")
    public String getUpdateAirlinePage(@RequestParam Integer id, Model model) {
        model.addAttribute("passengersList" , passengerService.getAll());
        model.addAttribute("ticket", ticketService.findById(id));
        return "ticketAction";
    }

    @GetMapping("/addTicket")
    public String getAddPage(Model model) {
        model.addAttribute("ticket", new Ticket());
        model.addAttribute("passengersList" , passengerService.getAll());
        return "ticketAction";
    }

    @GetMapping("/deleteTicket")
    public String delete(@RequestParam Integer id) {
        ticketService.delete(id);
        return "redirect:/ticket";
    }

    @PostMapping("/addTicket")
    public String create(@ModelAttribute("ticket") Ticket ticket, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        ticketService.save(ticket);
        return "redirect:/ticket";
    }

    @PostMapping("/updateTicket")
    public String getUpdateAirlinePage(@ModelAttribute("ticket") Ticket ticket, BindingResult result) {
        if (result.hasErrors()) {
            return "error";
        }
        ticketService.update(ticket);
        return "redirect:/ticket";
    }
}

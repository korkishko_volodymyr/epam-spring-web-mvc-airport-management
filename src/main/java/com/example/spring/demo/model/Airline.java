package com.example.spring.demo.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Airline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String code;
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "airline")
    private List<Plane> planes;
    @ManyToOne
    private Airport airport;
}

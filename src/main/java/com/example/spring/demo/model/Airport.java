package com.example.spring.demo.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String city;
    private String country;
    private String code;
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "airport")
    private List<Airline> airlines;
}

package com.example.spring.demo.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String date;
    private int duration;
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "flight")
    private List<Ticket> tickets;
    @ManyToOne
    private Plane plane;
}

package com.example.spring.demo.model;

import com.example.spring.demo.model.enums.Sex;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Passenger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;
    private Sex sex;
    private int age;
    private String phoneNumber;
    private String passportNumber;
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "passenger")
    private List<Ticket> tickets;
}

package com.example.spring.demo.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Plane {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String model;
    private int numbPlaces;
    @ManyToOne
    private Airline airline;
    @ToString.Exclude
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "plane")
    private List<Flight> flights;
}

package com.example.spring.demo.model;

import com.example.spring.demo.model.enums.ServiceClass;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String number;
    private ServiceClass serviceClass;
    private int price;
    @ManyToOne
    private Passenger passenger;
    @ManyToOne
    private Flight flight;
}

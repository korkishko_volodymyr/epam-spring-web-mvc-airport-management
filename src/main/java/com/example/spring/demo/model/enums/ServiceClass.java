package com.example.spring.demo.model.enums;

public enum  ServiceClass {
    ECONOMY, BUSINESS, COMFORT
}

package com.example.spring.demo.model.enums;

public enum Sex {
    MALE, FEMALE
}

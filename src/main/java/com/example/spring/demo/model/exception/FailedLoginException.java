package com.example.spring.demo.model.exception;

public class FailedLoginException extends Exception {
    public FailedLoginException(String message) {
        super(message);
    }
}

package com.example.spring.demo.repo;

import com.example.spring.demo.model.Airline;
import com.example.spring.demo.model.Airport;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AirlineRepository extends CrudRepository<Airline, Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Airline set name = :name, code = :code, airport = :airport where id = :id")
    void update(@Param("name") String name, @Param("code") String code, @Param("airport") Airport airport, @Param("id") Integer id);
}

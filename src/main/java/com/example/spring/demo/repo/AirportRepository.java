package com.example.spring.demo.repo;

import com.example.spring.demo.model.Airport;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Airport set name = :name,city = :city,country = :country, code = :code where id = :id")
    void update(@Param("name") String name, @Param("city") String city, @Param("country") String country,
                @Param("code") String code, @Param("id") Integer id);
}

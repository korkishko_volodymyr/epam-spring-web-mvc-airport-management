package com.example.spring.demo.repo;

import com.example.spring.demo.model.Flight;
import com.example.spring.demo.model.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Flight set date = :date, duration = :duration, plane = :plane where id = :id")
    void update(@Param("date") String date, @Param("duration") Integer duration,
                @Param("plane") Plane plane, @Param("id") Integer id);
}

package com.example.spring.demo.repo;

import com.example.spring.demo.model.Passenger;
import com.example.spring.demo.model.enums.Sex;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PassengerRepository extends CrudRepository<Passenger, Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Passenger set firstName = :firstName,lastName = :lastName,sex = :sex, age = :age," +
            "phoneNumber = :phoneNumber, passportNumber = :passportNumber where id = :id")
    void update(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("sex") Sex sex,
                @Param("age") Integer age, @Param("phoneNumber") String phoneNumber,
                @Param("passportNumber") String passportNumber, @Param("id") Integer id);
}

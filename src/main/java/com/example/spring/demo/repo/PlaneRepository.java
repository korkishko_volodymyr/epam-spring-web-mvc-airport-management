package com.example.spring.demo.repo;

import com.example.spring.demo.model.Airline;
import com.example.spring.demo.model.Plane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PlaneRepository extends JpaRepository<Plane,Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Plane set model = :model, numbPlaces = :numbPlaces, airline = :airline where id = :id")
    void update(@Param("model") String model, @Param("numbPlaces") Integer numbPlaces,
                @Param("airline") Airline airline, @Param("id") Integer id);
}

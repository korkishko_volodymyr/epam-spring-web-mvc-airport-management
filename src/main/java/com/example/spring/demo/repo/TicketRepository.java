package com.example.spring.demo.repo;

import com.example.spring.demo.model.Passenger;
import com.example.spring.demo.model.Ticket;
import com.example.spring.demo.model.enums.ServiceClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Ticket set number = :number, serviceClass = :serviceClass, price = :price, passenger = :passenger where id = :id")
    void update(@Param("number") String number, @Param("serviceClass") ServiceClass serviceClass,
                @Param("price") Integer price, @Param("passenger") Passenger passenger, @Param("id") Integer id);
}

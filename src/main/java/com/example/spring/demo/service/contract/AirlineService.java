package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Airline;

public interface AirlineService extends ModelService<Airline> {
}

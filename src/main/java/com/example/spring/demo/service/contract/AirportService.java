package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Airport;

public interface AirportService extends ModelService<Airport> {
}

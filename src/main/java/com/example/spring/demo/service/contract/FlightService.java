package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Flight;

public interface FlightService extends ModelService<Flight> {
}

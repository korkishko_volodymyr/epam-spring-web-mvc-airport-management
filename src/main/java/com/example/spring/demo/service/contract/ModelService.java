package com.example.spring.demo.service.contract;

import java.util.List;

public interface ModelService<T> {

    void save(final T model);

    List<T> getAll();

    T findById(Integer id);

    void delete(Integer id);

    void update(T model);
}

package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Passenger;

public interface PassengerService extends ModelService<Passenger>{
}

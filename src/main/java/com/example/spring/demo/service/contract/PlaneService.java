package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Plane;

public interface PlaneService extends ModelService<Plane> {
}

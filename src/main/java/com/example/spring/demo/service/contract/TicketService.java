package com.example.spring.demo.service.contract;

import com.example.spring.demo.model.Ticket;

public interface TicketService extends ModelService<Ticket> {
}

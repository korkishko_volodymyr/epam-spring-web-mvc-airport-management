package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Airline;
import com.example.spring.demo.repo.AirlineRepository;
import com.example.spring.demo.service.contract.AirlineService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirlineServiceImpl implements AirlineService {

    private final AirlineRepository airlineRepository;

    public AirlineServiceImpl(AirlineRepository airlineRepository) {
        this.airlineRepository = airlineRepository;
    }

    public void save(final Airline airline) {
        airlineRepository.save(airline);
    }

    public List<Airline> getAll() {
        return (List<Airline>) airlineRepository.findAll();
    }

    public Airline findById(Integer id) { return airlineRepository.findById(id).orElseThrow(); }

    public void delete(Integer id){ airlineRepository.deleteById(id); }

    public void update(Airline airline){ airlineRepository.update(airline.getName(),airline.getCode(),
            airline.getAirport(), airline.getId()); }
}

package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Airport;
import com.example.spring.demo.repo.AirportRepository;
import com.example.spring.demo.service.contract.AirlineService;
import com.example.spring.demo.service.contract.AirportService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportServiceImpl implements AirportService {

    private final AirportRepository airportRepository;

    public AirportServiceImpl(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    public void save(final Airport airport) { airportRepository.save(airport); }

    public List<Airport> getAll() {
        return (List<Airport>) airportRepository.findAll();
    }

    public Airport findById(Integer id) { return airportRepository.findById(id).orElseThrow(); }

    public void delete(Integer id) { airportRepository.deleteById(id); }

    public void update(Airport airport) {
        airportRepository.update(airport.getName(), airport.getCity(), airport.getCountry(), airport.getCode(), airport.getId());
    }
}

package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Flight;
import com.example.spring.demo.repo.FlightRepository;
import com.example.spring.demo.service.contract.FlightService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    private final FlightRepository flightRepository;

    public FlightServiceImpl(FlightRepository flightRepository) { this.flightRepository = flightRepository; }

    public void save(final Flight flight) {
        flightRepository.save(flight);
    }

    public List<Flight> getAll() {
        return flightRepository.findAll();
    }

    public void delete(Integer id) { flightRepository.deleteById(id); }

    public Flight findById(Integer id){ return flightRepository.findById(id).orElseThrow();}

    public void update(Flight flight) { flightRepository.update(flight.getDate(), flight.getDuration(), flight.getPlane(),
           flight.getId()); }
}

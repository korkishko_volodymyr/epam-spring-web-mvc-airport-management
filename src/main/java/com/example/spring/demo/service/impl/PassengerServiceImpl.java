package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Passenger;
import com.example.spring.demo.repo.PassengerRepository;
import com.example.spring.demo.service.contract.PassengerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PassengerServiceImpl implements PassengerService {

    private final PassengerRepository passengerRepository;

    public PassengerServiceImpl(PassengerRepository passengerRepository) {
        this.passengerRepository = passengerRepository;
    }

    public void save(final Passenger passenger) {
        passengerRepository.save(passenger);
    }

    public List<Passenger> getAll() {
        return (List<Passenger>) passengerRepository.findAll();
    }

    public void delete(Integer id) { passengerRepository.deleteById(id); }

    public Passenger findById(Integer id){ return passengerRepository.findById(id).orElseThrow();}

    public void update(Passenger passenger) {
        passengerRepository.update(passenger.getFirstName(), passenger.getLastName(), passenger.getSex(),
                passenger.getAge(), passenger.getPhoneNumber(), passenger.getPassportNumber(), passenger.getId());
    }
}

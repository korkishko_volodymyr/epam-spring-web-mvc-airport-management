package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Plane;
import com.example.spring.demo.repo.PlaneRepository;
import com.example.spring.demo.service.contract.PlaneService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaneServiceImpl implements PlaneService {

    private final PlaneRepository planeRepository;

    public PlaneServiceImpl(PlaneRepository planeRepository) {
        this.planeRepository = planeRepository;
    }

    public void save(final Plane plane) {
        planeRepository.save(plane);
    }

    public List<Plane> getAll() {
        return planeRepository.findAll();
    }

    public void delete(Integer id) { planeRepository.deleteById(id); }

    public Plane findById(Integer id){ return planeRepository.findById(id).orElseThrow(); }

    public void update(Plane plane) { planeRepository.update(plane.getModel(), plane.getNumbPlaces(), plane.getAirline(), plane.getId()); }
}

package com.example.spring.demo.service.impl;

import com.example.spring.demo.model.Ticket;
import com.example.spring.demo.repo.TicketRepository;
import com.example.spring.demo.service.contract.TicketService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    public TicketServiceImpl(TicketRepository ticketRepository) { this.ticketRepository = ticketRepository; }

    public void save(final Ticket ticket) {
        ticketRepository.save(ticket);
    }

    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    public void delete(Integer id) { ticketRepository.deleteById(id); }

    public Ticket findById(Integer id){ return ticketRepository.findById(id).orElseThrow();}

    public void update(Ticket ticket) { ticketRepository.update(ticket.getNumber(), ticket.getServiceClass(),
            ticket.getPrice(), ticket.getPassenger(), ticket.getId()); }
}

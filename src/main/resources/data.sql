INSERT INTO Airport VALUES (1 ,'Lviv', 'LWO', 'UA', 'Lviv International Airport'),(2 ,'Kentucky', 'KIA', 'USA', 'Louisville International Airport'),(3 ,'Hesse','FIA','GER','Frankfurt Airport'),(4 ,'Texas', 'DIA', 'USA','Dallas/Fort Worth International Airport'),(5 ,'Delhi','IGA', 'IND','Indira GandhiInternational Airport');
INSERT INTO Airline VALUES (1 ,'QAT','Qatar Airlines', 2),(2 ,'TAA','Turkish Airlines', 5),(3 ,'SUP','SkyUP', 4),(4 ,'UIA','Ukraine International',3),(5 ,'WZZ','WizzAir',1);
INSERT INTO Plane VALUES (1,'Boeing 777', 233,1),(2,'Airbus A-310', 332 ,2),(3,'Boeing 747', 332,5),(4,'Antonov AN-120', 128,4);
INSERT INTO Passenger VALUES (1 ,33,'Igor','Robinson', 'BK122331', '0988858661', 0), (2 , 22, 'Ivan','Boiko','BK433212', '0987788654', 0), (3 , 27,'Julia','Manko','BН776896','0988855432' , 1), (4 ,30, 'Maria','Fedyk','BН485763','0984765890', 1);
INSERT INTO Flight VALUES (1,'2020-04-09T12:00', 222,1), (2,'2020-04-13T11:56', 188,3), (3,'2020-04-23T18:33', 149,4), (4,'2020-04-09T22:35', 131,1);
INSERT INTO Ticket VALUES (1 ,'#3DG23', 100,2,2,1), (2,'#FF55H', 210, 1,4,4), (3, '#76JL6', 321, 0, 2,2), (4, '#56OIO', 213, 2, 3,2), (5, '#00L21', 178, 0,1,3);


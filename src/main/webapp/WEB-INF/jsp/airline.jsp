<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airline</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="smallTable">
        <caption><h2>List of Airlines</h2></caption>
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Base Airport</th>
            <th>Action</th>
        </tr>
        <c:forEach var="airline" items="${airlines}">
            <tr>
                <td><c:out value="${airline.name}"/></td>
                <td><c:out value="${airline.code}"/></td>
                <td><c:out value="${airline.airport.name}"/></td>
                <td>
                    <div style="margin-left: 23%">
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/updateAirline?id=<c:out value='${airline.id}' />"
                               class="editButton">Edit</a>
                        </div>
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/deleteAirline?id=<c:out value='${airline.id}' />"
                               class="deleteButton">Delete</a>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/addAirline"/>" class="addButton">
            Add new Airline
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/flight"/>" class="nextButton">
            Go to Flights
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Airline Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="airline">
        <table class="addTable">
            <caption><h2>Add airline</h2></caption>
            <tr>
                <th><form:label path="name">Name</form:label></th>
                <td><form:input path="name" type="text" size="30" required="required" value="${airline.name}"/></td>
            </tr>
            <tr>
                <th><form:label path="code">Code</form:label></th>
                <td><form:input path="code" type="text" size="30" required="required" value="${airline.code}"/></td>
            </tr>
            <tr>
                <th><form:label path="airport">Base Airport</form:label></th>
                <td>
                    <form:select path="airport">
                        <form:option value="${airline.airport.id}" selected="selected" disabled="disabled" hidden="hidden">${airline.airport.name}</form:option>
                        <c:forEach items="${airportList}" var="airport">
                            <form:option value="${airport.id}">${airport.name}</form:option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>

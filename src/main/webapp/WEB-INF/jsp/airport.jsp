<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airport</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="smallTable">
        <caption><h2>List of Airports</h2></caption>
        <tr>
            <th>Name</th>
            <th>City</th>
            <th>Country</th>
            <th>Code</th>
            <th>Action</th>
        </tr>
        <c:forEach var="airport" items="${airports}">
            <tr>
                <td><c:out value="${airport.name}"/></td>
                <td><c:out value="${airport.city}"/></td>
                <td><c:out value="${airport.country}"/></td>
                <td><c:out value="${airport.code}"/></td>
                <td>
                    <div style="margin-left: 23%">
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/updateAirport?id=<c:out value='${airport.id}' />"
                               class="editButton">Edit</a>
                        </div>
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/deleteAirport?id=<c:out value='${airport.id}' />"
                               class="deleteButton">Delete</a>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/addAirport"/>" class="addButton">
            Add new Airport
        </a>
        <a href="<c:url value="/airline"/>" class="nextButton">
            Go to Airlines
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/flight"/>" class="nextButton">
            Go to Flights
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Airport Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="airport">
        <table class="addTable">
            <caption><h2>Add Airport</h2></caption>
            <tr>
                <th><form:label path="name">Name</form:label></th>
                <td><form:input path="name" type="text" size="30" required="required" value="${airport.name}"/></td>
            </tr>
            <tr>
                <th><form:label path="city">City</form:label></th>
                <td><form:input path="city" type="text" size="30" required="required" value="${airport.city}"/></td>
            </tr>
            <tr>
                <th><form:label path="country">Country</form:label></th>
                <td>
                    <form:select path="country">
                        <form:option value="${airport.country}" selected="selected" disabled="disabled" hidden="hidden">${airport.country}</form:option>
                        <c:forEach items="${countries}" var="country">
                            <form:option value="${country.key}">${country.value}</form:option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            <tr>
                <th><form:label path="code">Code</form:label></th>
                <td><form:input path="code" type="text" size="30" required="required" value="${airport.code}"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>

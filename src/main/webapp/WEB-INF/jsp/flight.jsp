<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Flight</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="mainTable">
        <caption><h2>List of Flights</h2></caption>
        <tr>
            <th>Date</th>
            <th>Duration(min.)</th>
            <th>Plane</th>
            <th>Action</th>
        </tr>
        <c:forEach var="flight" items="${flights}">
            <tr>
                <td><c:out value="${flight.date}"/></td>
                <td><c:out value="${flight.duration}"/></td>
                <td><c:out value="${flight.plane.model}"/></td>
                <td>
                    <div style="margin-left: 33%">
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/updateFlight?id=<c:out value='${flight.id}'/>"
                               class="editButton">Edit</a>
                        </div>
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/deleteFlight?id=<c:out value='${flight.id}'/>"
                               class="deleteButton">Delete</a>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/addFlight"/>" class="addButton">
            Add new Flight
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
        <a href="<c:url value="/airline"/>" class="nextButton">
            Go to Airlines
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/plane"/>" class="nextButton">
            Go to Planes
        </a>
    </div>
</div>
</body>
</html>

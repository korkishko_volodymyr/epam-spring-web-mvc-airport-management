<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Flight Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="flight">
        <table class="addTable">
            <caption><h2>Add Flight</h2></caption>
            <tr>
                <th><form:label path="date">Date</form:label></th>
                <td><form:input path="date" type="datetime-local" size="30" required="required" value="${flight.date}"/></td>
            </tr>
            <tr>
                <th><form:label path="duration">FlightDuration(min)</form:label></th>
                <td><form:input path="duration" type="number" min="0" size="30" required="required" value="${flight.duration}"/></td>
            </tr>

            <tr>
                <th><form:label path="plane">Plane</form:label></th>
                <td>
                    <form:select path="plane" required="required">
                        <form:option value="${flight.plane.id}" selected="selected" disabled="disabled" hidden="hidden">${flight.plane.model}</form:option>
                        <c:forEach items="${planeList}" var="plane">
                            <form:option value="${plane.id}">${plane.model}</form:option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>

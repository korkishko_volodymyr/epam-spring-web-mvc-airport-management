<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Airport Management</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="admin">
        <table class="addTable">
            <caption><h2>Login as admin*</h2></caption>
            <tr>
                <th><form:label path="login">Login:</form:label></th>
                <td><form:input path="login" type="text" size="30" required="required" value="${admin.login}"/></td>
            </tr>
            <tr>
                <th><form:label path="password">Password:</form:label></th>
                <td><form:input path="password" type="text" size="30" required="required" value="${admin.password}"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Login" class="addButton"/>
                </td>
            </tr>
        </table>
        <h4 style="text-align: center"><i>*to login read readme.md</i></h4>
        <c:if test="${message != null}">
            <div style="text-align: center;color: #ff3333">
                <h3>${message}</h3>
            </div>
        </c:if>
    </form:form>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Passenger Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="passenger">
        <table class="addTable">
            <caption><h2>Add Passenger</h2></caption>
            <tr>
                <th><form:label path="firstName">FirstName</form:label></th>
                <td><form:input path="firstName" type="text" size="30" required="required" value="${passenger.firstName}"/></td>
            </tr>
            <tr>
                <th><form:label path="lastName">LastName</form:label></th>
                <td><form:input path="lastName" type="text" size="30" required="required" value="${passenger.lastName}"/></td>
            </tr>
            <tr>
                <th><form:label path="age">Age</form:label></th>
                <td><form:input path="age" type="number" size="30" required="required" value="${passenger.age}"/></td>
            </tr>
            <tr>
                <th><form:label path="phoneNumber">Phone</form:label></th>
                <td><form:input path="phoneNumber" type="text" size="30" required="required" value="${passenger.phoneNumber}"/></td>
            </tr>
            <tr>
                <th><form:label path="sex">Sex</form:label></th>
                <td>
                    <form:select path="sex">
                        <form:option value="MALE">Male</form:option>
                        <form:option value="FEMALE">Female</form:option>
                    </form:select>
                </td>
            </tr>
            <tr>
                <th><form:label path="passportNumber">Passport</form:label></th>
                <td><form:input path="passportNumber" type="text" size="30" required="required" value="${passenger.passportNumber}"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Plane</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <table class="smallTable">
        <caption><h2>List of Planes</h2></caption>
        <tr>
            <th>Mark</th>
            <th>Places</th>
            <th>Airline(Owner)</th>
            <th>Action</th>
        </tr>
        <c:forEach var="plane" items="${planes}">
            <tr>
                <td><c:out value="${plane.model}"/></td>
                <td><c:out value="${plane.numbPlaces}"/></td>
                <td><c:out value="${plane.airline.name}"/></td>
                <td>
                    <div style="margin-left: 28%">
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/updatePlane?id=<c:out value='${plane.id}' />"
                               class="editButton">Edit</a>
                        </div>
                        <div style="float: left;">
                            <a href="${pageContext.request.contextPath}/deletePlane?id=<c:out value='${plane.id}' />"
                               class="deleteButton">Delete</a>
                        </div>
                    </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <div class="addTable">
        <a href="<c:url value="/addPlane"/>" class="addButton">
            Add new Plane
        </a>
        <a href="<c:url value="/airline"/>" class="nextButton">
            Go to Airlines
        </a>
        <a href="<c:url value="/ticket"/>" class="nextButton">
            Go to Tickets
        </a>
        <a href="<c:url value="/flight"/>" class="nextButton">
            Go to Flights
        </a>
        <a href="<c:url value="/passenger"/>" class="nextButton">
            Go to Passengers
        </a>
        <a href="<c:url value="/airport"/>" class="nextButton">
            Go to Airports
        </a>
    </div>
</div>
</body>
</html>

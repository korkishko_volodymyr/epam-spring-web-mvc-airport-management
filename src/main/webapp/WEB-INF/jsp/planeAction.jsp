<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Plane Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="plane">
        <table class="addTable">
            <caption><h2>Add Plane</h2></caption>
            <tr>
                <th><form:label path="model">Model</form:label></th>
                <td><form:input path="model" type="text" size="30" required="required"  value="${plane.model}"/></td>
            </tr>
            <tr>
                <th><form:label path="numbPlaces">Places</form:label></th>
                <td><form:input path="numbPlaces" type="number" size="30" required="required" value="${plane.numbPlaces}"/></td>
            </tr>
            <tr>
                <th><form:label path="airline">Airline</form:label></th>
                <td>
                    <form:select path="airline">
                        <form:option value="${plane.airline.id}" selected="selected" disabled="disabled" hidden="hidden">${plane.airline.name}</form:option>
                        <c:forEach items="${airlineList}" var="airline">
                            <form:option value="${airline.id}">${airline.name}</form:option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>

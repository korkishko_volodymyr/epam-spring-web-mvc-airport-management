<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Ticket Action</title>
    <style type="text/css">
        <%@include file="/styles/style.css"%>
    </style>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="ticket">
        <table class="addTable">
            <caption><h2>Add Ticket</h2></caption>
            <tr>
                <th><form:label path="number">Number</form:label></th>
                <td><form:input path="number" type="text" size="30" required="required" value="${ticket.number}"/></td>
            </tr>
            <tr>
                <th><form:label path="serviceClass">ServiceClass</form:label></th>
                <td>
                    <form:select path="serviceClass">
                        <form:option value="ECONOMY">ECONOMY</form:option>
                        <form:option value="BUSINESS">BUSINESS</form:option>
                        <form:option value="COMFORT">COMFORT</form:option>
                    </form:select>
                </td>
            </tr>
            <tr>
                <th><form:label path="price">Price(USD)</form:label></th>
                <td><form:input path="price" type="number" min="0" size="30" required="required" value="${ticket.price}"/></td>
            </tr>
            <tr>
                <th><form:label path="passenger">Passenger</form:label></th>
                <td>
                    <form:select path="passenger">
                        <form:option value="${ticket.passenger.id}" selected="selected" disabled="disabled" hidden="hidden">${ticket.passenger.firstName} ${ticket.passenger.lastName}</form:option>
                        <c:forEach items="${passengersList}" var="passenger">
                            <form:option value="${passenger.id}">${passenger.firstName} ${passenger.lastName}</form:option>
                        </c:forEach>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Save" class="addButton"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
